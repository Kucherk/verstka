$(document).ready(function(){
    /*---upperSlider---*/ 
    var slideIndex = 1;

    function sliderButtonsPush(){
        showSlides(slideIndex);
        $("button.prev").click(function(){
            showSlides(--slideIndex);
        });
        $("button.next").click(function(){
            showSlides(++slideIndex);
        });
    }

    function currentSlide(){
        var dots = $(".dot");
        dots.each(function(){
           $(this).click(function(){
                showSlides(slideIndex = dots.index(this)+1);
           });
        });
    }

    function showSlides(n) {
        var i;
        var slides = $(".mySlides");
        var dots = $(".dot");
        if (n > slides.length) {slideIndex = 1} 
        if (n < 1) {slideIndex = slides.length};
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none"; 
        }
        slides[slideIndex-1].style.display = "block"; 
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        dots[slideIndex-1].className += " active";
    }

    /*---scrollBar---*/
    function scrollGalery(){
        var videoLenght    = $('.video').width();
        var numberOfVideos = $('.video').length;
        var windowLenght   = $('#videos').width();
        var marginRight    = parseInt( $('.video').css('margin-right'), 10);
        var galleryLenght  = (videoLenght + marginRight) * numberOfVideos - windowLenght;
        $('#scroll').attr('max', galleryLenght);
        var change = $('#scroll').val();
        $('#videos').css({"margin-left": -change + 'px'});
    };
    
    $("#scroll").change(function(){
        scrollGalery();
    });
    
    $("#scroll").on("input", function(){
        scrollGalery();
    });
    
    /*---Video Lightning---*/
    $('.video a .boldFont').hover(function(){
        $(this).parent().prev().css('background','linear-gradient( to top right, #fff, #fff)').children().css('borderLeftColor','#43AA26');
        },function(){
        $(this).parent().prev().css('background','linear-gradient( to top right, #43aa26, #a2c166)').children().css('borderLeftColor','#fff'); 
    }); 

    /*---Gallery---*/
    function sliderAction(){
        var count = 0;
        var gallerySize = $('#galleryCont').width();
        var itemSum = $('.item').length;
        var itemSize = $('.item').width();
        var itemMarg = parseInt( $('.item').css('margin-right'), 10);
        var itemPadd = parseInt( $('.item').css('padding-right'), 10);
        var item     = itemSize + itemMarg + (itemPadd*2);
        var sliderSize = (itemSize + itemMarg) * itemSum;
        var hiddenSlides = sliderSize - gallerySize;

        $('#b2').click(function(){
            ++count;
            var galleryMarg = parseInt( $('#galleryCont').css('margin-left'), 10);
            if(count <= itemSum-gallerySize/item){
                var btn = $(this).prop('disabled', true);
                $('#galleryCont').animate(
                    {"margin-left": "-=" + item + "px"}, 
                    {
                        duration: 500, 
                        complete: function(){
                            btn.prop('disabled', false);
                        }
                    });

            }else{
                --count;
            }
        });

        $('#b1').click(function(){
            --count;
            var galleryMarg = parseInt( $('#galleryCont').css('margin-left'), 10);
            if (count >= 0){
                var btn = $(this).prop('disabled', true);
                $('#galleryCont').animate(
                    {"margin-left": "+=" + item + "px"},
                    {
                        duration: 500,
                        complete: function(){
                            btn.prop('disabled', false);
                        }
                    });
            }else{
                ++count;
            }
        });
    }

    /*---GaleryList---*/

    $('#gallery li').each(function(){
        $(this).click(function(){
            $('#gallery li').each(function(){
                $(this).css('border','2px solid #fff').children().css('color','#2f3131');
            });

            $(this).css('border','2px solid #43aa26').children().css('color','#43aa26');
        }); 
    });

    /*---map---*/

    function displayMap(){
        var latitude = 49.239609;
        var longitude = 28.502778;
        var mapDiv = document.getElementById('map');
        var googleCoords = new google.maps.LatLng(latitude, longitude);
        var mapOptions = {
            zoom : 15,
            center : googleCoords,
            mapTypeId : google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapDiv, mapOptions);
        var marker = new google.maps.Marker(
                    {position : googleCoords, 
                     map : map,          
                     title :'Our shop',     
                     clickable : true});

        var infoWindow = new google.maps.InfoWindow(
                         {content : 'this is my furniture shop',       
                         position : googleCoords});
        google.maps.event.addListener(marker, "click", function(){
            infoWindow.open(map);    
        });
    }

    /*---downSlider---*/
    function downSlider(){
        var count = 0;
        var smallImages = $(".gImage");
        var largeImages = $(".dImage");
        $('#b4').click(function(){
            count++;
            $('.gImage').each(function(){
                $(this).removeClass('activeImg'); 
            });
            if (count < smallImages.length){
                smallImages[count].classList.add('activeImg');
            }else{
                count = 0;
                smallImages[count].classList.add('activeImg');
            }

            $('.display img').each(function(){
                $(this).removeClass('visible');
            });

            largeImages[count].classList.add('visible');
        });

        $('#b3').click(function(){
            count--;
            $('.gImage').each(function(){
                $(this).removeClass('activeImg'); 
            });
            if (count >= 0){
                smallImages[count].classList.add('activeImg');
            }else{
                count = smallImages.length-1;
                smallImages[count].classList.add('activeImg');
            }

            $('.display img').each(function(){
                $(this).removeClass('visible');
            });
            largeImages[count].classList.add('visible');
        });
    }

    /*---pop-up info---*/
    function popUpInfo(){
        var flag = false;
        $('#popUp').click(function(){
            if(!flag){
                flag = true;
                $('#delInfo').css('display','block');
            }else{
                flag = false;
                $('#delInfo').css('display','none');
            } 
        });
    }


    sliderButtonsPush();
    currentSlide();
    sliderAction(); 
    displayMap();
    popUpInfo();
    downSlider();
});


