$(document).ready(function(){
    function mainSlider(){
        var counter = 0;
        
        function showMainSlider(){
            var slides = $(".mainSlide");
            if (counter < 0){
                counter = slides.length-1;
            }else if(counter>=slides.length){
                counter = 0; 
            }
            slides.each(function(){
                $(this).css('display', 'none');
                if($(this).index() == counter) {
                    $(this).css('display', 'block');
                }
            });
        }

        $(".nextMainSl").click(function(){
            counter++;
            showMainSlider();
        });
        
        $(".prevMainSl").click(function(){
            counter--;
            showMainSlider();
        });
        
        showMainSlider();
    }
    
    
    
    
    
    mainSlider();
});